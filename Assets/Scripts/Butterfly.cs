﻿using UnityEngine;

public enum Color
{
    Black,
    Red,
    White,
    Purple,
    Blue
}

public enum Direction
{
    Left,
    Right
}
public class Butterfly : MonoBehaviour
{

    public float _speed = 1.0f;

    public Region[] _regions;

    public float _durationRotation;

    public float _angle;

    public int _current;

    private Region _currentRegion;

    private Vector3 _direction;

    private Vector3 _newPosition;

    private bool _isChangeState;

    private Vector3 _nextStatePosition;

    private Quaternion _endRotation;

    private Quaternion _startRotation;

    private float _timeRotation;

    private float _squareAngleEdge1;

    private float _hypotenuse;

    private float _squareAngleEdge2;

    private Vector3 _middle;

    private Vector3 destination = Vector3.zero;

    private Plane _lastPlane;



    private Vector3 _rotation;

    private Vector3 test;

    private bool _rotate;

    private Direction _lastDirection;

    // Use this for initialization
    void Start()
    {
        _nextStatePosition = Vector3.zero;

        _currentRegion = _regions[_current];

        _rotation = transform.eulerAngles;

        _newPosition = transform.position;

        getNextStatePosition();

        _lastPlane = _currentRegion.plane;
    }

    // Update is called once per frame
    void Update()
    {
        //_startRotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);



        if (_rotate)
        {

            //_newPosition += _direction * Time.deltaTime * _speed;


            _timeRotation += Time.deltaTime;

            transform.rotation = Quaternion.Slerp(_startRotation, _endRotation, _timeRotation / _durationRotation);

            switch (_lastPlane)
            {
                case Plane.XY:
                    if (_lastDirection == Direction.Left)
                    {
                        if (transform.eulerAngles.y == 270)
                        {
                            _rotate = false;
                            _lastPlane = Plane.YZ;
                            _newPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);

                            _timeRotation = 0;
                        }
                    }
                    else
                    {
                        if (transform.eulerAngles.y == 90)
                        {
                            _rotate = false;
                            _lastPlane = Plane.YZ;
                        }
                    }
                    break;
                case Plane.YZ:
                    if (_lastDirection == Direction.Left)
                    {
                        if (transform.eulerAngles.y == 180)
                        {
                            _rotate = false;
                            _lastPlane = Plane.XY1;
                            //_newPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);

                            _timeRotation = 0;
                        }
                    }
                    else
                    {
                        if (transform.eulerAngles.y == 90)
                        {
                            _rotate = false;
                            _lastPlane = Plane.XY;
                        }
                    }
                    break;
                case Plane.XY1:
                    if (_lastDirection == Direction.Left)
                    {
                        if (transform.eulerAngles.y == 90)
                        {
                            _rotate = false;

                            //_newPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
                            _lastPlane = Plane.YZ1;
                            _timeRotation = 0;
                        }
                    }
                    else
                    {
                        if (transform.eulerAngles.y == 90)
                        {
                            _rotate = false;
                            _lastPlane = Plane.XY;
                        }
                    }
                    break;
                case Plane.YZ1:
                    if (_lastDirection == Direction.Left)
                    {
                        if (transform.eulerAngles.y == 0)
                        {
                            _rotate = false;

                            //_newPosition = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z);
                            _lastPlane = Plane.XY;
                            _timeRotation = 0;
                        }
                    }
                    else
                    {
                        if (transform.eulerAngles.y == 90)
                        {
                            _rotate = false;
                            _lastPlane = Plane.XY;
                        }
                    }
                    break;

                case Plane.XZ:

                    break;
            }
        }

        switch (_currentRegion.plane)
        {
            # region Plane.XY
            case Plane.XY:
                _newPosition += _direction * Time.deltaTime * _speed;

                transform.position = _newPosition;

                if (!_rotate)
                {
                    if (transform.position.x >= _currentRegion._maxDistanceRotate)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, -90, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Left;
                    }
                    else if (transform.position.x <= _currentRegion._minDistanceRotate)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, 90, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Right;
                    }
                }


                if (transform.position.x >= _currentRegion._maxDistanceTranslate)
                {
                    _current = 1;
                    _middle = transform.position;

                    changeState();

                    if (_angle > 270 && _angle < 360)
                    {
                        _middle.z = _currentRegion._maxDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 360 - transform.eulerAngles.z);
                    }
                    else if (_angle > 180 && _angle <= 270)
                    {
                        _middle.z = _currentRegion._maxDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }
                else if (transform.position.x <= _currentRegion._minDistanceTranslate && _angle > 0 && _angle <= 180)
                {
                    _current = 1;
                    _middle = transform.position;

                    changeState();

                    if (_angle > 90 && _angle <= 180)
                    {
                        _middle.z = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }
                    else
                    {
                        _middle.z = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }

                break;
            #endregion

            #region Plane.XY1
            case Plane.XY1:
                Debug.Log("_rotate " + _rotate);
                _newPosition += _direction * Time.deltaTime * _speed;

                transform.position = _newPosition;

                if (!_rotate)
                {
                    if (transform.position.x >= _currentRegion._maxDistanceRotate && _angle > 0 && _angle <= 180)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, -270, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Right;
                    }
                    else if (transform.position.x <= _currentRegion._minDistanceRotate && _angle > 180 && _angle <= 360)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, -270, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Left;
                    }
                }


                if (transform.position.x >= _currentRegion._maxDistanceTranslate && !(_angle > 180 && _angle <= 360))
                {
                    _current = 3;
                    _middle = transform.position;

                    changeState();

                    if (_angle > 270 && _angle < 360)
                    {
                        _middle.z = _currentRegion._maxDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                        //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 360 - transform.eulerAngles.z);
                    }
                    else if (_angle > 180 && _angle <= 270)
                    {
                        _middle.z = _currentRegion._maxDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }
                else if (transform.position.x <= _currentRegion._minDistanceTranslate && !(_angle > 0 && _angle <= 180))
                {
                    _current = 3;
                    _middle = transform.position;

                    changeState();

                    if (_angle > 90 && _angle <= 180)
                    {
                        _middle.z = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }
                    else
                    {
                        _middle.z = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }

                break;
            #endregion

            #region Plane.XZ
            case Plane.XZ:
                //_newPosition += _direction * Time.deltaTime * _speed;

                //transform.position = _newPosition;

                //if (!_rotate)
                //{
                //    if (transform.position.z > _currentRegion._maxDistanceRotate && _angle > 180 && _angle <= 360)
                //    {
                //        _startRotation = transform.rotation;

                //        _endRotation = Quaternion.Euler(transform.eulerAngles.x, -270, transform.eulerAngles.z);

                //        _rotate = true;

                //        _lastDirection = Direction.Left;
                //    }
                //    else if (transform.position.z <= _currentRegion._minDistanceRotate)
                //    {
                //        _startRotation = transform.rotation;

                //        _endRotation = Quaternion.Euler(transform.eulerAngles.x, 270, transform.eulerAngles.z);

                //        _rotate = true;

                //        _lastDirection = Direction.Right;
                //    }
                //}
                //if (_angle > 180 && _angle <= 360)
                //{
                //    if (transform.position.z > _currentRegion._maxDistanceTranslate)
                //    {
                //        _current = 3;
                //        _middle = transform.position;

                //        changeState();

                //        if (_angle > 270 && _angle <= 360)
                //        {
                //            _middle.z = _currentRegion._minDistance;

                //            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                //            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                //            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                //            destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                //            _direction = destination - transform.position;

                //            _direction = _direction.normalized;
                //        }
                //        else if (_angle > 180 && _angle <= 270)
                //        {
                //            _middle.z = _currentRegion._maxDistance;

                //            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                //            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                //            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                //            destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                //            _direction = destination - transform.position;

                //            _direction = _direction.normalized;
                //        }

                //    }
                //}
                //else if (transform.position.z < _currentRegion._minDistanceTranslate)
                //{
                //    _current = 3;
                //    _middle = transform.position;

                //    changeState();

                //    if (_angle > 90 && _angle <= 180)
                //    {
                //        _middle.x = _currentRegion._minDistance;

                //        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                //        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                //        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                //        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                //        _direction = destination - transform.position;

                //        _direction = _direction.normalized;
                //    }
                //    else
                //    {
                //        _middle.x = _currentRegion._minDistance;

                //        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                //        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                //        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                //        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                //        _direction = destination - transform.position;

                //        _direction = _direction.normalized;
                //    }

                //}

                break;
            #endregion

            #region Plane.YZ
            case Plane.YZ:
                _newPosition += _direction * Time.deltaTime * _speed;

                transform.position = _newPosition;

                if (!_rotate)
                {
                    if (transform.position.z > _currentRegion._maxDistanceRotate && _angle > 180 && _angle <= 360)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, -180, transform.eulerAngles.z);

                        _rotate = true;

                        _lastDirection = Direction.Left;
                    }
                    else if (transform.position.z <= _currentRegion._minDistanceRotate && _angle > 0 && _angle <= 180)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, 90, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Right;
                    }
                }
                if (_angle > 180 && _angle <= 360)
                {
                    if (transform.position.z > _currentRegion._maxDistanceTranslate)
                    {

                        _current = 2;
                        _middle = transform.position;

                        changeState();

                        if (_angle > 270 && _angle <= 360)
                        {
                            _middle.x = _currentRegion._minDistance;

                            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                            destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                            _direction = destination - transform.position;

                            _direction = _direction.normalized;
                        }
                        else if (_angle > 180 && _angle <= 270)
                        {
                            _middle.x = _currentRegion._maxDistance;

                            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                            destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                            _direction = destination - transform.position;

                            _direction = _direction.normalized;
                        }

                    }
                }
                else if (transform.position.z < _currentRegion._minDistanceTranslate)
                {
                    _current = 2;

                    _middle = transform.position;

                    changeState();

                    if (_angle > 90 && _angle <= 180)
                    {
                        _middle.x = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }
                    else
                    {
                        _middle.x = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }
                break;
            #endregion

            #region Plane.YZ1
            case Plane.YZ1:
                _newPosition += _direction * Time.deltaTime * _speed;

                transform.position = _newPosition;

                if (!_rotate)
                {
                    if (transform.position.z > _currentRegion._maxDistanceRotate && _angle > 0 && _angle <= 180)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);

                        _rotate = true;

                        _lastDirection = Direction.Right;
                    }
                    else if (transform.position.z <= _currentRegion._minDistanceRotate && _angle > 180 && _angle <= 360)
                    {
                        _startRotation = transform.rotation;

                        _endRotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);

                        _rotate = true;
                        _lastDirection = Direction.Left;
                    }
                }
                if (_angle > 180 && _angle <= 360)
                {
                    if (transform.position.z < _currentRegion._minDistanceTranslate)
                    {

                        _current = 0;
                        _middle = transform.position;

                        changeState();

                        if (_angle > 270 && _angle <= 360)
                        {
                            _middle.x = _currentRegion._maxDistance;

                            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                            destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                            _direction = destination - transform.position;

                            _direction = _direction.normalized;
                        }
                        else if (_angle > 180 && _angle <= 270)
                        {
                            _middle.x = _currentRegion._maxDistance;

                            _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                            _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                            _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                            destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                            _direction = destination - transform.position;

                            _direction = _direction.normalized;
                        }

                    }
                }
                else if (transform.position.z > _currentRegion._maxDistanceTranslate)
                {
                    _current = 2;

                    _middle = transform.position;

                    changeState();

                    if (_angle > 90 && _angle <= 180)
                    {
                        _middle.x = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }
                    else
                    {
                        _middle.x = _currentRegion._minDistance;

                        _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                        _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                        _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                        destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                        _direction = destination - transform.position;

                        _direction = _direction.normalized;
                    }

                }
                break;
                #endregion
        }
    }
    void getNextStatePosition()
    {
        _middle = transform.position;

        switch (_currentRegion.plane)
        {
            case Plane.XY:
                if (_angle > 270 && _angle < 360)
                {
                    _middle.x = _currentRegion._maxDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else if (_angle > 180 && _angle <= 270)
                {
                    _middle.x = _currentRegion._maxDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else if (_angle > 90 && _angle <= 180)
                {
                    _middle.x = _currentRegion._minDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else
                {
                    _middle.x = _currentRegion._minDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                break;
            case Plane.XY1:

                break;
            case Plane.XZ:

                break;
            case Plane.YZ:

                if (_angle > 270 && _angle < 360)
                {
                    _middle.x = _currentRegion._maxDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (360 - _angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else if (_angle > 180 && _angle <= 270)
                {
                    _middle.x = _currentRegion._maxDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle - 180));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else if (_angle > 90 && _angle <= 180)
                {
                    _middle.x = _currentRegion._minDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (180 - _angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y - _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                else
                {
                    _middle.x = _currentRegion._minDistance;

                    _squareAngleEdge1 = Vector3.Distance(transform.position, _middle);

                    _hypotenuse = _squareAngleEdge1 / Mathf.Sin(Mathf.Deg2Rad * (_angle));

                    _squareAngleEdge2 = Mathf.Sqrt(_hypotenuse * _hypotenuse - _squareAngleEdge1 * _squareAngleEdge1);

                    destination = new Vector3(_middle.x, _middle.y + _squareAngleEdge2, _middle.z);

                    _direction = destination - transform.position;

                    _direction = _direction.normalized;
                }
                break;
            case Plane.YZ1:

                break;
        }

        
    }
    void changeState()
    {
        if (_current < 0)
        {
            _current = _regions.Length - 1;
        }
        if (_current == _regions.Length)
        {
            _current = 0;
        }
        _isChangeState = true;

        _rotate = true;

        _currentRegion = _regions[_current];


    }
    Vector3 getPointWithTwoPointsZ(Vector3 A, Vector3 B, float cLen, Vector3 angle)
    {

        Vector3 B2 = B - A;

        Quaternion angleSpin = Quaternion.Euler(angle);

        Vector3 C2 = angleSpin * B2;

        return C2.normalized * cLen + B;
    }
    Vector3 getPointWithTwoPointsY(Vector3 A, Vector3 B, float cLen, float angle)
    {

        Vector3 B2 = B - A;

        Quaternion angleSpin = Quaternion.Euler(angle, 0, 0);

        Vector3 C2 = angleSpin * B2;

        return C2.normalized * cLen + B;
    }
}
