﻿using UnityEngine;
using System.Collections;

public enum Plane
{
    XY,
    XY1,
    YZ,
    YZ1,
    XZ,

}
public class Region : MonoBehaviour
{

    public Plane plane;

    public float _minDistanceRotate;

    public float _maxDistanceRotate;



    public float _minDistanceTranslate;

    public float _maxDistanceTranslate;

    public float _minDistance;

    public float _maxDistance;

    public float _angle;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
